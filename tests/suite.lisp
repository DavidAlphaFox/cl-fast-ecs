(in-package #:cl-fast-ecs/tests)

(define-test cl-fast-ecs)

(defparameter *enable-colors* NIL)

(defun run ()
  (let* ((interactivep (or (find :slynk *features*)
                           (find :swank *features*)))
         (*enable-colors* (not interactivep))
         (report (test (if uiop:*command-line-arguments*
                           (mapcar #'(lambda (a)
                                       (alexandria:format-symbol
                                        'cl-fast-ecs/tests "~:@(~a~)" a))
                                   uiop:*command-line-arguments*)
                           'cl-fast-ecs)
                       :report
                       (if interactivep 'interactive 'plain))))
    #+sbcl (when (uiop:getenvp "COVERAGE")
             (handler-bind ((warning #'muffle-warning))
               (sb-cover:report "coverage/")))
    (unless interactivep
      (uiop:quit (if (eq :failed (status report)) 1 0) NIL))
    report))

(defparameter *regular* 39)
(defparameter *green* 32)
(defparameter *red* 31)

(defun color-text (color text)
  (if *enable-colors*
      (format nil "[0;~am~a[0;39m" color text)
      text))

(defun status-text (result text)
  (color-text
   (case (status result)
     (:passed *green*)
     (:failed *red*)
     (otherwise *regular*))
   text))

(defmethod parachute:format-result ((result result) (type (eql :oneline)))
  (status-text result (parachute::print-oneline (expression result) NIL)))

(defmethod parachute:format-result ((result test-result) (type (eql :oneline)))
  (let ((*print-case* :downcase)
        (name (name (expression result))))
    (status-text result (format NIL "~a" name))))
