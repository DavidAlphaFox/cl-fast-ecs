(defpackage :test-component+c
  (:use :cl :cl-fast-ecs))
(in-package :test-component+c)

(cl-fast-ecs:defcomponent pos
  "Location information"
  (x 0 :type fixnum :documentation "X coordinate")
  (y 0 :type fixnum :documentation "Y coordinate"))

(defpackage :test-component+s
  (:use :cl :cl-fast-ecs))
(in-package :test-component+s)

(defsystem move
  (:components-rw (test-component+c::pos))
  "Moves objects."
  (incf pos-x)
  (incf pos-y))
