(defpackage :test-component-default
  (:use :cl :cl-fast-ecs))
(in-package :test-component-default)

(defun cool-func (x)
  (declare (fixnum x))
  (random (1+ x)))

(defcomponent rnd
  (x 0 :type fixnum)
  (y (cool-func x) :type fixnum))
