(in-package #:cl-fast-ecs/tests)


(defmacro ignore-warnings (&body body)
  `(handler-bind
       ((warning #'muffle-warning))
     (locally ,@body)))

#-ecs-unsafe
(define-test component-redefinition
  :parent cl-fast-ecs
  :depends-on (components entities storage systems hooks)
  (bind-storage)
  (let* ((entity (make-entity))
         (x 42.0)
         (y 42.9)
         (z 0.1))
    (declare (special entity x y z))
    (ignore-warnings
     (eval  ;; HACK : simulating sly-compile-defun
      '(defcomponent coordinate
        (x 0.0 :type single-float)
        (y 0.0 :type single-float)
        (z 0.0 :type single-float))))
    (is eq :tag (first ecs::*component-registry*))
    (is eq :position (third ecs::*component-registry*))
    (is eq :velocity (fifth ecs::*component-registry*))
    (eval
     '(progn
       (make-coordinate entity :x x :y y :z z)
       (is = x (coordinate-x entity))
       (is = y (coordinate-y entity))
       (is = z (coordinate-z entity))))))
