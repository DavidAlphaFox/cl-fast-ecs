(defsystem "cl-fast-ecs"
  :version "0.6.0"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "MIT"
  :homepage "https://gitlab.com/lockie/cl-fast-ecs"
  :depends-on (:alexandria :trivial-garbage :trivial-adjust-simple-array)
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "components"
                  :depends-on ("hooks" "index"))
                 (:file "entities"
                  :depends-on ("components" "hooks" "index" "storage"))
                 (:file "hooks")
                 (:file "index"
                  :depends-on ("storage"))
                 (:file "storage"
                  :depends-on ("hooks"))
                 (:file "systems"
                  :depends-on ("entities" "storage")))))
  :description "Blazingly fast Entity-Component-System microframework."
  :in-order-to ((test-op (test-op "cl-fast-ecs/tests"))))

(defsystem "cl-fast-ecs/tests"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "MIT"
  :depends-on (:cl-fast-ecs :parachute :cl-mock-basic #+sbcl :sb-cover)
  :serial t
  :components ((:module "tests"
                :components
                ((:file "package")
                 (:file "suite")
                 (:file "hooks")
                 (:file "components")
                 (:file "entities")
                 (:file "storage")
                 (:file "systems")
                 (:file "component-redefinition"))))
  :description "Test system for cl-fast-ecs"
  :perform (test-op (op c) (uiop:symbol-call :cl-fast-ecs/tests '#:run)))
