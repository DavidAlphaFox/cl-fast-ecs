(in-package #:cl-user)

(defpackage cl-fast-ecs
  (:documentation "**NOTE: this software is of alpha quality, and the API is
subject to change.**

`cl-fast-ecs` is a Common Lisp library providing an
implementation of the Entity-Component-System pattern, primarily focused
on speed and interactive development.

ECS is an architectural data-oriented design pattern that allows for the
effective processing of a large number of in-game objects while keeping the
code and data separated. This provides flexibility in the way that game
objects are built at runtime.

NOTE: to get the maximum performance, but loose the ability to interactively
redefine components in the storage, add `:ECS-UNSAFE` keyword into `*FEATURES*`
before loading the library.")
  (:nicknames #:ecs)
  (:use #:cl #:trivial-adjust-simple-array)
  (:import-from #:alexandria
                #:array-index #:array-length #:compose #:deletef #:ensure-symbol
                #:format-symbol #:if-let #:iota #:length= #:make-gensym
                #:make-gensym-list #:make-keyword #:once-only #:parse-body
                #:positive-fixnum #:symbolicate #:when-let #:with-gensyms)
  (:import-from #:trivial-garbage #:make-weak-pointer #:weak-pointer-value)
  ;; components.lisp
  (:export
   #:*component-defined-hook*
   #:*component-redefined-hook*
   #:*component-storage-grown-hook*
   #:make-component
   #:delete-component
   #:replace-component
   #:print-component
   #:defcomponent
   #:define-component)
  ;; entities.lisp
  (:export
   #:entity-valid-p
   #:*entity-storage-grown-hook*
   #:make-entity
   #:*entity-deleting-hook*
   #:delete-entity
   #:copy-entity
   #:make-object
   #:spec-adjoin
   #:*skip-printing-components*
   #:print-entity
   #:print-entities/picture)
  ;; hooks.lisp
  (:export
   #:hook-up
   #:unhook)
  ;; storage.lisp
  (:export
   #:entity
   #:make-storage
   #:*storage*
   #:bind-storage)
  ;; systems.lisp
  (:export
   #:defsystem
   #:define-system
   #:current-entity
   #:system-exists-p
   #:delete-system
   #:run-systems))
