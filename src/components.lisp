(in-package :cl-fast-ecs)


(declaim (type list *component-registry*))
(defvar *component-registry* nil)
(declaim (type array-length *component-registry-length*))
(defvar *component-registry-length* 0)

(defhook :component-defined
  :documentation
  "Called when a new component is defined with `DEFCOMPONENT` macro. Arguments
for the call are the index in the component registry and the constructor
 function for component storage (not to be called directly by user).

See `HOOK-UP`.")

(defhook :component-redefined
  :documentation
  "Called when an existing component is redefined with `DEFCOMPONENT` macro.
Arguments for the call are the index in the component registry and the
constructor function for component storage (not to be called directly by user).

See `HOOK-UP`.")

(defhook :component-storage-grown
  :documentation
  "Called after the component storate capacity is increased due to the component
being added to the new entity. Arguments for the call are the index in the
component registry and the new storage capacity.

Note: this is a good place to call for a full GC cycle to collect old storage
arrays.

See `HOOK-UP`.")

(defgeneric make-component (index entity &key &allow-other-keys)
  (:documentation "Create a component on a given `ENTITY` in generic fashion.

Not recommended for using in a tight loops such as systems, see `DEFCOMPONENT`
documentation for alternatives."))

(defgeneric delete-component (index entity)
  (:documentation "Deletes a compionent on a given `ENTITY` in generic fashion.

Not recommended for using in a tight loops such as systems, see `DEFCOMPONENT`
documentation for alternatives."))

(defgeneric replace-component (index source-entity dest-entity)
  (:documentation "Copies component slot values from given `SOURCE-ENTITY` to
given `DEST-ENTITY` in generic fashion.


Not recommended for using in a tight loops such as systems, see `DEFCOMPONENT`
documentation for alternatives."))

(defgeneric print-component (index entity &optional stream)
  (:documentation "Prints component data to the given `STREAM`.

See `PRINT-ENTITY`."))

(declaim (ftype (function (symbol string list list list list symbol) list)
                %soa-decl))
(defun %soa-decl (name documentation slot-names slot-defaults slot-types
                  slot-index composite-index-name)
  (let* ((struct-name (symbolicate name :-soa))
         (actual-struct-name (gentemp (string (the symbol struct-name))))
         (default-decls (mapcar #'list slot-names slot-defaults))
         (soa-slots (mapcar #'(lambda (name type)
                                `(,name (make-array
                                         0
                                         :initial-element
                                         ;; HACK for compilers that require
                                         ;; DEFSTRUCT to be on the toplevel
                                         ;; (looking at you, CCL)
                                         (let* (,@default-decls)
                                           (declare (ignorable ,@slot-names))
                                           ,name)
                                         :element-type ',type)
                                        :type (simple-array ,type (*))))
                            slot-names slot-types))
         (soa-index-slots
           (mapcar
            #'(lambda (name index)
                (when index
                  `(,(symbolicate name :-index)
                    (make-array 0
                                :element-type
                                '(or null (simple-array fixnum (*)))
                                :initial-element nil)
                    :type
                    (simple-array (or null (simple-array fixnum (*))) (*)))))
            slot-names slot-index))
         (adjust-decls (mapcar #'(lambda (name type)
                                   (let ((a (symbolicate
                                             :% struct-name :- name)))
                                     `(adjust-simple-arrayf
                                       (,a soa) new-size
                                       :element-type ',type
                                       :initial-element ,name)))
                               slot-names slot-types))
         (fill-slots
           `(setf ,@(mapcan
                     #'(lambda (name type)
                         (declare (ignorable type))
                         `((gethash ,(make-keyword name) slots)
                           ;; NOTE: ABCL and CLisp always box values in arrays
                           #+(or abcl clisp)
                           (cons
                            (,(symbolicate :% struct-name :- name) soa) ',type)
                           #-(or abcl clisp)
                           (,(symbolicate :% struct-name :- name) soa)))
                     slot-names slot-types))))
    `(progn
       #-ecs-unsafe
       (declaim
        (notinline
         ,@(mapcar (lambda (n) (symbolicate :% struct-name :- n)) slot-names)
         ,@(mapcar (lambda (n) `(setf ,(symbolicate :% struct-name :- n)))
                   slot-names)))
       (declaim (ftype (function (,actual-struct-name array-length))
                       ,(symbolicate :%adjust- struct-name)))
       (defstruct (,actual-struct-name
                   (:conc-name ,(symbolicate :% struct-name :-))
                   (:constructor ,(symbolicate :%make- struct-name :*))
                   (:copier nil)
                   (:predicate nil)
                   (:include component-soa))
         ,documentation
         ,@soa-slots ,@(remove nil soa-index-slots)
         ,@(when composite-index-name
             `((,(symbolicate composite-index-name :-index)
                (make-array 0
                            :element-type '(or null (simple-array fixnum (*))))
                :type (simple-array (or null (simple-array fixnum (*))) (*))))))
       (defun ,(symbolicate :%make- struct-name) (entities-allocated
                                                  old-component-storage)
         (cond
           (old-component-storage
            (let* ((allocated (component-soa-allocated old-component-storage))
                   (old-slots (component-soa-slots old-component-storage))
                   (soa
                     (,(symbolicate :%make- struct-name :*)
                      :allocated allocated
                      :exists
                      (component-soa-exists old-component-storage)
                      :min-entity
                      (component-soa-min-entity old-component-storage)
                      :max-entity
                      (component-soa-max-entity old-component-storage)
                      :count
                      (component-soa-count old-component-storage)
                      ,@(mapcan
                         #'(lambda (name type)
                             (let ((array-decl
                                     `(make-array
                                       allocated
                                       :initial-element
                                       (let* (,@default-decls)
                                         (declare (ignorable ,@slot-names))
                                         ,name)
                                       :element-type ',type)))
                               `(,(make-keyword name)
                                 (let ((old-slot (gethash ,(make-keyword name)
                                                          old-slots)))
                                   #+(or abcl clisp)
                                   (if (and old-slot (eq ',type (cdr old-slot)))
                                       (car old-slot)
                                       ,array-decl)
                                   #-(or abcl clisp)
                                   (if (and old-slot
                                            (eq ',type
                                                (array-element-type old-slot)))
                                       old-slot
                                       ,array-decl)))))
                         slot-names slot-types)))
                   (slots (component-soa-slots soa)))
              (declare (ignorable old-slots slots))
              ,fill-slots
              soa))
           (t
            (let* ((soa (,(symbolicate :%make- struct-name :*)
                         :exists (make-array entities-allocated
                                             :element-type 'bit
                                             :initial-element 0)))
                   (slots (component-soa-slots soa)))
              (declare (ignorable slots))
              ,fill-slots
              soa))))
       (defun ,(symbolicate :%adjust- struct-name) (soa new-size)
         (declare (optimize (safety 0)))
         (let* (,@default-decls)
           ,@adjust-decls)
         (let ((slots (component-soa-slots soa)))
           (declare (ignorable slots))
           ,fill-slots)
         (setf (component-soa-allocated soa) new-size))
       nil)))

(declaim (inline %non-unique-error))
(defun %non-unique-error (name values slots)
  (let ((composite-p (and (second values) t)))
    (error
     "~:[Value~;Tuple~] ~{~s~^, ~} is not unique among ~a values ~{~a~^, ~}"
     composite-p values name slots)))

(declaim
 (ftype
  (function (symbol symbol symbol symbol symbol (or null symbol) boolean) list)
  %update-index))
(defun %update-index (name slot soa data value index unique)
  (when index
    `(let ((index (,(symbolicate :% name :-soa- slot :-index) ,soa)))
       (index-delete index entity ((aref ,data entity)))
       ,(if unique
            `(when (index-insert index (,data) entity (,value) t)
               (%non-unique-error ',name (list ,value) '(,slot)))
            `(index-insert index (,data) entity (,value) nil)))))

(declaim
 (ftype
  (function (symbol symbol symbol symbol list list symbol list boolean) list)
  %update-composite-index))
(defun %update-composite-index (name slot soa value slot-names slot-types
                                composite-index-name composite-index-slots
                                composite-index-unique)
  (when composite-index-name
    (let* ((data-names (make-gensym-list (length composite-index-slots)))
           (values (mapcar #'(lambda (d s) (if (eq s slot) value `(aref ,d entity)))
                           data-names composite-index-slots)))
      `(let ((index (,(symbolicate :% name :-soa- composite-index-name :-index)
                     ,soa))
             ,@(mapcar #'(lambda (n s type)
                           `(,n (the (simple-array ,type)
                                     (,(symbolicate :% name :-soa- s) ,soa))))
                       data-names composite-index-slots
                       (map 'list
                            #'(lambda (s)
                                (declare (type symbol s))
                                (nth (the array-index (position s slot-names))
                                     slot-types))
                            composite-index-slots)))
         (index-delete index entity
             ,(mapcar #'(lambda (d) `(aref ,d entity)) data-names))
         ,(if composite-index-unique
              `(when (index-insert index ,data-names entity ,values t)
                 (%non-unique-error ',name (list ,@values)
                                    ',composite-index-slots))
              `(index-insert index ,data-names entity ,values nil))))))

(declaim
 (ftype
  (function (symbol symbol list list list list list symbol list boolean) list)
  %accessor-decls))
(defun %accessor-decls (name index-var-name slot-names slot-types slot-docs
                        slot-index slot-unique composite-index-name
                        composite-index-slots composite-index-unique)
  (mapcan
   #'(lambda (n type doc i u)
       (let ((accessor-name (symbolicate name :- n))
             (slot-accessor-name (symbolicate :% name :-soa- n)))
         `((declaim #+ecs-unsafe (inline ,accessor-name (setf ,accessor-name))
                    (ftype (function (entity) ,type) ,accessor-name)
                    (ftype (function (,type entity) ,type)
                           (setf ,accessor-name)))
           (defun ,accessor-name (entity)
             ,doc
             (let* ((component-storages (storage-component-storages *storage*))
                    (soa (svref component-storages ,index-var-name)))
               (aref (the (simple-array ,type) (,slot-accessor-name soa))
                     entity)))
           (defun (setf ,accessor-name) (value entity)
             ,doc
             (let* ((component-storages (storage-component-storages *storage*))
                    (soa (svref component-storages ,index-var-name))
                    (data (the (simple-array ,type) (,slot-accessor-name soa))))
               ,(%update-index name n 'soa 'data 'value i u)
               ,(%update-composite-index
                 name n 'soa 'value slot-names slot-types composite-index-name
                 composite-index-slots composite-index-unique)
               (setf (aref data entity) value))))))
   slot-names slot-types slot-docs slot-index slot-unique))

(declaim
 (ftype (function
         (symbol symbol list list list list list symbol list boolean) list)
        %ctor-decl))
(defun %ctor-decl (name index-var-name slot-names slot-defaults slot-types
                   slot-index slot-unique composite-index-name
                   composite-index-slots composite-index-unique)
  `(defun ,(symbolicate :make- name) (entity &key ,@(mapcar #'list
                                                            slot-names
                                                            slot-defaults))
     "Add component with given values to an entity."
     (maybe-check-entity entity)
     (let* ((component-storages (storage-component-storages *storage*))
            (soa (svref component-storages ,index-var-name)))
       (if (zerop (sbit (component-soa-exists soa) entity))
           (block new
             (when (>= entity (component-soa-allocated soa))
               (let ((new-size (new-capacity (1+ entity))))
                 (,(symbolicate :%adjust- name :-soa) soa new-size)
                 (run-hook *component-storage-grown-hook*
                           ,index-var-name new-size)))
             (incf (component-soa-count soa))
             ,@(remove
                nil
                (mapcar
                 #'(lambda (s type i u)
                     (when i
                       `(let ((index
                                (,(symbolicate :% name :-soa- s :-index) soa))
                              (data
                                (the (simple-array ,type)
                                     (,(symbolicate :% name :-soa- s) soa))))
                          (when-let (new-index
                                     (index-maybe-grow
                                         index
                                         (component-soa-count soa)
                                         (data)
                                         (component-soa-exists soa)
                                         (component-soa-min-entity soa)
                                         (component-soa-max-entity soa)))
                            (setf index new-index
                                  (,(symbolicate :% name :-soa- s :-index) soa)
                                  new-index))
                          ,(if u
                               `(when (index-insert index (data) entity (,s) t)
                                  (%non-unique-error ',name (list ,s) '(,s)))
                               `(index-insert index (data) entity (,s) nil)))))
                 slot-names slot-types slot-index slot-unique))
             ,(when composite-index-name
                (let ((data-names (make-gensym-list
                                   (length composite-index-slots))))
                  `(let ((index (,(symbolicate
                                   :% name :-soa- composite-index-name :-index)
                                 soa))
                         ,@(mapcar
                             #'(lambda (n s type)
                                 `(,n (the (simple-array ,type)
                                           (,(symbolicate :% name :-soa- s)
                                            soa))))
                             data-names
                             composite-index-slots
                             (map 'list
                                  #'(lambda (s)
                                      (declare (type symbol s))
                                      (nth (the array-index
                                                (position s slot-names))
                                           slot-types))
                                  composite-index-slots)))
                     (when-let (new-index (index-maybe-grow
                                              index
                                              (component-soa-count soa)
                                              ,data-names
                                              (component-soa-exists soa)
                                              (component-soa-min-entity soa)
                                              (component-soa-max-entity soa)))
                       (setf index new-index
                             (,(symbolicate
                                :% name :-soa- composite-index-name :-index)
                              soa)
                             new-index))
                     ,(if composite-index-unique
                          `(when (index-insert index ,data-names entity
                                     ,composite-index-slots t)
                             (%non-unique-error
                              ',name (list ,@composite-index-slots)
                              ',composite-index-slots))
                          `(index-insert index ,data-names entity
                               ,composite-index-slots nil)))))
             (when (< entity (component-soa-min-entity soa))
               (setf (component-soa-min-entity soa) entity))
             (when (> entity (component-soa-max-entity soa))
               (setf (component-soa-max-entity soa) entity))
             (setf (sbit (component-soa-exists soa) entity) 1
                   (sbit (storage-component-created-bits *storage*)
                         ,index-var-name) 1))
           (block exists
             #-ecs-unsafe
             (warn "component ~a already exists on entity ~a" ',name entity)))
       (setf ,@(mapcan
                #'(lambda (s type)
                    `((aref (the (simple-array ,type)
                                 (,(symbolicate :% name :-soa- s) soa))
                            entity) ,s))
                slot-names slot-types)))
     nil))

(declaim
 (ftype
  (function (symbol symbol list list list (or null cons) list symbol list) list)
  %dtor-decl))
(defun %dtor-decl (name index-var-name slot-names slot-defaults slot-types
                   finalize slot-index composite-index-name
                   composite-index-slots)
  (let ((default-decls (mapcar #'list slot-names slot-defaults)))
    `(let* (,@default-decls)
       (declare (ignorable ,@slot-names))
       (defun ,(symbolicate :delete- name) (entity)
         (maybe-check-entity entity)
         (let* ((component-storages (storage-component-storages *storage*))
                (soa (svref component-storages ,index-var-name)))
           ,(when finalize
              `(funcall ,finalize
                        entity
                        ,@(mapcan
                          #'(lambda (s type)
                              `(,(make-keyword s)
                                (aref (the (simple-array ,type)
                                           (,(symbolicate :% name :-soa- s)
                                            soa))
                                      entity)))
                          slot-names slot-types)))
           ,@(remove nil
                     (mapcar
                      #'(lambda (s type index)
                          (when index
                            `(index-delete
                              (,(symbolicate :% name :-soa- s :-index) soa)
                              entity
                              ((aref (the (simple-array ,type)
                                          (,(symbolicate :% name :-soa- s)
                                           soa))
                                     entity)))))
                      slot-names slot-types slot-index))
           ,(when composite-index-name
              `(index-delete
                (,(symbolicate :% name :-soa- composite-index-name :-index) soa)
                entity
                ,(mapcar
                  #'(lambda (s type)
                      `(aref (the (simple-array ,type)
                                  (,(symbolicate :% name :-soa- s) soa))
                             entity))
                  composite-index-slots
                  (map 'list
                       #'(lambda (s)
                           (declare (type symbol s))
                           (nth (the array-index (position s slot-names))
                                slot-types))
                       composite-index-slots))))
           (decf (component-soa-count soa))
           (setf (sbit (component-soa-exists soa) entity) 0
                 (sbit (storage-component-removed-bits *storage*)
                       ,index-var-name) 1
                 #-ecs-unsafe
                 ,@(mapcan
                    #'(lambda (s type)
                        `((aref (the (simple-array ,type)
                                     (,(symbolicate :% name :-soa- s) soa))
                                entity) ,s))
                    slot-names slot-types)))
         nil))))

(declaim (inline format-symbol/component))
(defun format-symbol/component (component control &rest args)
  (let ((package (symbol-package component)))
    (apply #'format-symbol
           (if (eq package (find-package :common-lisp))
               t
               package)
           control
           args)))

(declaim
 (ftype (function (symbol symbol list list list list symbol list boolean) list)
        %with-macro-decl))
(defun %with-macro-decl (name index-var-name slot-names slot-types
                         slot-index slot-unique composite-index-name
                         composite-index-slots composite-index-unique)
  (let ((slot-gensyms (mapcar #'(lambda (s) (declare (type symbol s))
                                  (gensym (string s)))
                              slot-names)))
    `(defmacro ,(symbolicate :%with- name :-slots)
         (bindings readonly component-storages entity &body body)
       (with-gensyms (soa)
         (let* ((soa-slot-bindings
                  (mapcar #'(lambda (n s type)
                              `(,n (the (simple-array ,type)
                                        (,(format-symbol/component
                                           ',name "%~a-SOA-~a" ',name s)
                                         ,soa))))
                          ',slot-gensyms ',slot-names ',slot-types))
                (accessor-names
                  (mapcar #'(lambda (s) (gentemp (format nil "%~a" s)))
                          ',slot-names))
                (setter-names (mapcar #'(lambda (n) `(setf ,n)) accessor-names))
                (accessor-bindings
                  (mapcan
                   #'(lambda (n s slot type i u)
                       `((,n (entity) (aref ,s entity))
                         ,@(when #-ecs-unsafe (not readonly) #+ecs-unsafe t
                                 `(((setf ,n) (v entity)
                                    (declare (ignorable v entity)
                                             (type ,type v))
                                    ,(if readonly
                                         `(error "~a slot ~a is readonly"
                                                 ',',name ',slot)
                                         `(progn
                                            ,(%update-index
                                              ',name slot soa s 'v i u)
                                            ,(%update-composite-index
                                              ',name slot soa 'v
                                              ',slot-names ',slot-types
                                              ',composite-index-name
                                              ',composite-index-slots
                                              ,composite-index-unique)
                                            (setf (aref ,s entity) v))))))))
                   accessor-names ',slot-gensyms ',slot-names ',slot-types
                   ',slot-index ',slot-unique))
                (binding-names (or bindings
                                   (mapcar #'(lambda (n) (symbolicate ',name :- n))
                                           ',slot-names)))
                (slot-bindings
                  (mapcar #'(lambda (n a) `(,n (,a ,entity)))
                          binding-names accessor-names)))
           `(let* ((,soa (svref ,component-storages ,',index-var-name))
                   ,@soa-slot-bindings)
              (declare (ignorable ,soa ,@',slot-gensyms)
                       #+sbcl (sb-ext:muffle-conditions
                               sb-ext:code-deletion-note))
              (flet (,@accessor-bindings)
                (declare (inline ,@accessor-names
                                 ,@(unless readonly setter-names))
                         #+sbcl (sb-ext:unmuffle-conditions
                                 sb-ext:code-deletion-note)
                         #+clasp (ignorable ,@accessor-names ,@setter-names))
                (symbol-macrolet (,@slot-bindings)
                  ,@(if #+ecs-unsafe nil #-ecs-unsafe readonly
                        `((handler-case (progn ,@body)
                            (undefined-function (u)
                              (let ((f (cell-error-name u)))
                                (if (and (eq (first f) 'setf)
                                         (find (second f)
                                               ',binding-names :test #'eq))
                                    (error "~a slot ~a is readonly"
                                           ',',name (second f))
                                    (error u))))))
                        body)))))))))

(declaim
 (ftype (function (symbol symbol symbol symbol (or symbol cons) symbol boolean)
                  list)
        %index-accessor-decls))
(defun %index-accessor-decls (component-name index-var-name struct-name name
                              type index unique)
  (when index
    (if unique
        `(progn
           (declaim
            (ftype (function (,type &key (:missing-error-p boolean)) entity)
                   ,index))
           (defun ,index (value &key (missing-error-p t))
             (let* ((component-storages (storage-component-storages *storage*))
                    (soa (svref component-storages ,index-var-name))
                    (data (,(symbolicate :% struct-name :- name) soa)))
               (declare (type (simple-array ,type) data))
               (if-let (entity (index-lookup-1
                                (,(symbolicate :% struct-name :- name :-index)
                                 soa)
                                (data) (value)))
                 entity
                 (if missing-error-p
                     (error "There is no entity with ~a ~a ~a"
                            ',component-name ',name value)
                     -1)))))
        `(progn
           (declaim (ftype (function (,type &key (:count array-length)
                                            (:start array-index)) list) ,index))
           (defun ,index (value &key (count (- array-dimension-limit 2))
                                     (start 0))
             (let* ((component-storages (storage-component-storages *storage*))
                    (soa (svref component-storages ,index-var-name))
                    (data (,(symbolicate :% struct-name :- name) soa)))
               (declare (type (simple-array ,type) data))
               (index-lookup
                   (,(symbolicate :% struct-name :- name :-index) soa)
                   (data) (value) count start)))
           (defmacro ,(symbolicate :with- index) (value entity &body body)
             (once-only (value)
               (with-gensyms (component-storages soa data)
                 `(let* ((,component-storages
                           (storage-component-storages *storage*))
                         (,soa (svref ,component-storages ,',index-var-name))
                         (,data (,(symbolicate :% ',struct-name :- ',name)
                                 ,soa)))
                    (declare (type (simple-array ,',type) ,data))
                    (index-enumerate
                        (,(symbolicate :% ',struct-name :- ',name :-index) ,soa)
                        (,data) (,value) ,entity ,@body)))))))))

(declaim (ftype (function (symbol symbol list boolean list list list) list)
                %composite-index-accessor-decls))
(defun %composite-index-accessor-decls (name struct-name slots unique
                                        slot-names slot-defaults slot-types)
  (let* ((composite-index-slot-types
           (map 'list
                #'(lambda (s)
                    (declare (type symbol s))
                    (nth (position s slot-names) slot-types))
                slots))
         (composite-index-slot-defaults
           (map 'list
                #'(lambda (s)
                    (declare (type symbol s))
                    (nth (position s slot-names) slot-defaults))
                slots))
         (keyword-decls (mapcar #'(lambda (n type) (list (make-keyword n) type))
                                slots composite-index-slot-types))
         (keyword-supplied (mapcar #'(lambda (n) (symbolicate n :-supplied-p))
                                   slots))
         (keyword-args (mapcar
                        #'(lambda (n d s) (list n d s))
                        slots composite-index-slot-defaults keyword-supplied))
         (index-var-name (format-symbol :cl-fast-ecs "+~a-COMPONENT-INDEX+"
                                        struct-name))
         (data-names (make-gensym-list (length slots)))
         (data-bindings (mapcar
                         #'(lambda (d s type)
                             `(,d
                               (the (simple-array ,type)
                                    (,(symbolicate :% struct-name :-soa- s)
                                     soa))))
                         data-names slots composite-index-slot-types)))
    (when name
      (when (find :missing-error-p slot-names :test #'string=)
        (error
         "Unsupported slot name for the composite index: MISSING-ERROR-P"))
      `(progn
         (declaim
          (ftype (function (&key (:missing-error-p boolean) ,@keyword-decls)
                           ,(if unique 'entity 'list))
                 ,name))
         (defun ,name (&key (missing-error-p t) ,@keyword-args)
           (unless (and ,@keyword-supplied)
             (error
              "Missing slot values in composite index accessor: ~{~a~^, ~}"
              (let ((keyword-supplied (list ,@keyword-supplied)))
                (declare (dynamic-extent keyword-supplied))
                (loop :for s :of-type boolean :in keyword-supplied
                      :for n :of-type symbol :in ',slots
                      :unless s :collect n))))
           (let* ((component-storages (storage-component-storages *storage*))
                  (soa (svref component-storages ,index-var-name))
                  ,@data-bindings
                  (result (,(if unique 'index-lookup-1 'index-lookup)
                           (,(symbolicate :% struct-name :-soa- name :-index)
                            soa)
                           ,data-names ,slots)))
             (declare (type ,(if unique '(or entity null) 'list) result))
             ,(if unique
                  `(if result result
                       (if missing-error-p
                           (error
                            "There is no entity with ~a ~{~a~^, ~} = ~{~a~^, ~}"
                            ',struct-name ',slots (list ,@slots))
                           -1))
                  `result)))))))

(defmacro defcomponent (name* &rest slots*)
  "Defines component structure with `NAME` and `SLOTS`, allowing optional
docstring, just like `DEFSTRUCT`.

There's extra keyword argument `:INDEX` to the slot definition, which, if set
to symbol, creates a hash table-based index for that slot allowing
fast (amort. *O(1)*) lookups in form of \"which set of entities have this
exact slot value\"; that might come in handy when implementing parent/child or
other entity relationships. The downside of such index is that component
add/delete/update operations take a little bit longer, and addition can
trigger expensive table rehashing.

Additionally, when `:UNIQUE` is set to `T`, such index ensures that entities
and given slot values map 1 to 1, at least in value's `SXHASH` sense. This
might prove valuable when implementing entity names.

To be called from a top-level. Allows redefinition of component with the same
`NAME` but different set of `SLOT`s; all necessary storage will be properly
reallocated.

This macro defines a following set of operations for given `NAME` *name*:

* an internal structure called `name-SOA` with its required internal machinery;
* a constant `+name-COMPONENT-INDEX` to reference component data within a storage;
* a component data accessors `name-slotName` and
`(SETF name-slotName)` for every slot on a component, with *O(1)*
complexity (their usage is discouraged though because of poor caching
performance, in favour of defining a new system with `DEFSYSTEM`);
* a component constructor aptly named `MAKE-name`, which adds the component
being defined to the given `ENTITY`, with *O(k)* complexity, where *k* is the
number of slots on the component;
* specialization of `MAKE-COMPONENT` generic on a component being defined;
* a predicate called `HAS-name-P` testing whether given `ENTITY` has the
component being defined, with *O(1)* complexity;
* a component destructor called `DELETE-name`, removing the component being
defined from the given `ENTITY` in *O(k)* complexity,where *k* is the number
of slots on the component;
* a convenience macro `WITH-name` akin to `WITH-SLOTS`;
* an upsert operator `ASSIGN-name` which first ensures the component exists
on given entity and then sets its values according to given keyword arguments,
but keeps slots that weren't specified to their previous values;
* a `REPLACE-name` function which copies the component slot values from one
given entity to another;
* a `name-COUNT` helper function with no arguments which returns the current
count of entities having this component;
* some internal helper macros;
* if `:INDEX` if set to a symbol for any slot, the function with the name
denoted by that symbol is defined, taking slot value as an argument and
returning the list of entities having such slot value;
* if `:UNIQUE` is specified and set to `T` in addition to `:INDEX`, the index
function returns a single entity having given slot value, or raises a condition
if there's no such entity. If the `:MISSING-ERROR-P` keyword argument to that
function is `NIL`, no condition is raised, but negative entity is returned
instead.

Also runs hook `*COMPONENT-DEFINED-HOOK*` or `*COMPONENT-REDEFINED-HOOK*` when
called, depending on circumstances."
  (let* ((name (if (symbolp name*) name* (first name*)))
         finalize*
         composite-index-name composite-index-slots composite-index-unique
         (has-documentation-p (typep (first slots*) 'string))
         (documentation (if has-documentation-p (first slots*) ""))
         (slots (if has-documentation-p (rest slots*) slots*))
         (struct-name (symbolicate name :-soa))
         (slot-names (mapcar #'car slots))
         (slot-defaults (mapcar #'cadr slots))
         (slot-types (mapcar #'(lambda (s) (getf s :type t)) slots))
         (slot-index (mapcar #'(lambda (s) (getf s :index nil)) slots))
         (slot-unique (mapcar #'(lambda (s) (getf s :unique nil)) slots))
         (_ (unless (every #'(lambda (i u) (or i (not u))) slot-index slot-unique)
              (error "The :UNIQUE argument requires :INDEX to be set")))
         (slot-docs (mapcar #'(lambda (s) (getf s :documentation "")) slots))
         (keyword-decls (mapcar #'(lambda (n type) (list (make-keyword n) type))
                                slot-names slot-types))
         (index-var-name (format-symbol :cl-fast-ecs "+~a-COMPONENT-INDEX+"
                                        name)))
    (declare (ignore _)
             (type list composite-index-slots))
    (loop
     :for slot-name :in slot-names
     :for slot-type :in slot-types
     :do (cond
           ((or (subtypep slot-type 'string)
                (subtypep slot-type 'symbol)
                (and (symbolp slot-type)
                     (string= slot-type 'foreign-pointer)))
            ;; NOTE: those types indeed are boxed, but no need to rub it in
            nil)
           ((or (subtypep slot-type 'array)
                (subtypep slot-type 'sequence))
            (uiop:style-warn
             "~a values will be boxed; consider using separate entities instead"
             slot-name))
           ((subtypep slot-type 'hash-table)
            (uiop:style-warn
             "~a values will be boxed; consider using slot index instead"
             slot-name))
           ((eq t (upgraded-array-element-type slot-type))
            (uiop:style-warn
             "~a values will be boxed; consider using primitive type"
             slot-name))))
    (unless (symbolp name*)
      (destructuring-bind (n &key finalize composite-index) name*
        (declare (ignore n))
        (setf finalize* finalize)
        (when composite-index
          (destructuring-bind (name slots &key unique) composite-index
            (setf composite-index-name name
                  composite-index-slots slots
                  composite-index-unique unique)
            (assert
             (> (length composite-index-slots) 1)
             (composite-index-slots)
             "Composite index requires >= 2 slots, got ~a instead (~{~a~^, ~})"
             (length composite-index-slots)
             composite-index-slots)
            (assert
             (not (set-difference composite-index-slots slot-names))
             (composite-index-slots)
             "Unknown slot(s): ~{~a~^, ~}"
             (nreverse (set-difference composite-index-slots slot-names)))))))
    `(progn
       (eval-when (:compile-toplevel :load-toplevel :execute)
         ,(%soa-decl name documentation slot-names slot-defaults slot-types
                     slot-index composite-index-name)
         #+abcl (declaim (special ,index-var-name))
         (unless (boundp ',index-var-name)
           (declaim (type array-index ,index-var-name))
           (defconstant ,index-var-name *component-registry-length*)
           (incf *component-registry-length*))
         (setf (getf *component-registry* ,(make-keyword name))
               #',(symbolicate :%make- struct-name)))
       (run-hook (if (boundp ',index-var-name)
                     *component-redefined-hook*
                     *component-defined-hook*)
                 ,index-var-name #',(symbolicate :%make- struct-name))
       ,@(%accessor-decls name index-var-name slot-names slot-types slot-docs
                          slot-index slot-unique composite-index-name
                          composite-index-slots composite-index-unique)
       (declaim (ftype (function (entity &key ,@keyword-decls))
                       ,(symbolicate :make- name)))
       ,(%ctor-decl name index-var-name slot-names slot-defaults slot-types
                    slot-index slot-unique composite-index-name
                    composite-index-slots composite-index-unique)
       (defmethod make-component ((index (eql ,index-var-name)) entity
                                  &rest args
                                  &key ,@(mapcar #'list
                                                 slot-names
                                                 slot-defaults))
         (declare (ignorable ,@slot-names))
         (apply #',(symbolicate :make- name) entity args))
       (declaim (ftype (function (entity) boolean)
                       ,(symbolicate :has- name :-p)))
       (defun ,(symbolicate :has- name :-p) (entity)
         (maybe-check-entity entity)
         (let* ((component-storages (storage-component-storages *storage*))
                (soa (svref component-storages ,index-var-name)))
           (not (zerop (sbit (component-soa-exists soa) entity)))))
       (declaim (ftype (function (entity))
                       ,(symbolicate :delete- name)))
       ,(%dtor-decl name index-var-name slot-names slot-defaults slot-types
                    finalize* slot-index composite-index-name
                    composite-index-slots)
       (defmethod delete-component ((index (eql ,index-var-name)) entity)
         (funcall #',(symbolicate :delete- name) entity))
       ,(%with-macro-decl name index-var-name slot-names slot-types
                          slot-index slot-unique composite-index-name
                          composite-index-slots composite-index-unique)
       (defmacro ,(symbolicate :with- name) (bindings entity &body body)
         "A convenience macro to access entity's component data.
Prefer DEFSYSTEM to access the component data though."
         (with-gensyms (entity-var)
           (let ((internal-macro ',(symbolicate :%with- name :-slots))
                 (slot-names ',slot-names))
             `(progn
                (let ((,entity-var ,entity))
                  (declare (ignorable ,entity-var))
                  (maybe-check-entity ,entity-var)
                  (,internal-macro ,(or bindings slot-names) nil
                                   (storage-component-storages *storage*)
                                   ,entity-var
                                   ,@body))))))
       (defun ,(symbolicate :assign- name)
           (entity &rest args
                   &key ,@(mapcar #'(lambda (n d)
                                      (list n d (symbolicate n :-supplied-p)))
                                  slot-names slot-defaults))
         "Creates or updates the component."
         (maybe-check-entity entity)
         (let* ((component-storages (storage-component-storages *storage*))
                (soa (svref component-storages ,index-var-name)))
           (declare (ignorable soa))
           (if (zerop (sbit (component-soa-exists soa) entity))
               (apply #',(symbolicate :make- name) entity args)
               ,(let ((slot-names* (mapcar (compose 'gensym 'string)
                                           slot-names)))
                  `(,(symbolicate :%with- name :-slots) ,slot-names* nil
                    component-storages entity
                    ,@(mapcar #'(lambda (n* n) `(when ,(symbolicate n :-supplied-p)
                                             (setf ,n* ,n)))
                       slot-names* slot-names)
                    nil)))))
       ,@(when (every #'null slot-unique)
          `((defun ,(symbolicate :replace- name) (dst-entity src-entity)
              (declare (ignorable src-entity))
              (maybe-check-entity dest-entity)
              (maybe-check-entity src-entity)
              (,(symbolicate :%with- name :-slots) ,slot-names t
               (storage-component-storages *storage*)
               src-entity
               (,(symbolicate :assign- name)
                dst-entity
                ,@(mapcan #'(lambda (n) (list (make-keyword n) n)) slot-names)))
              dst-entity)
            (defmethod replace-component ((index (eql ,index-var-name))
                                          dst-entity src-entity)
              (declare (ignorable index))
              (funcall #',(symbolicate :replace- name) dst-entity src-entity))))
       (defmethod print-component ((index (eql ,index-var-name)) entity
                                   &optional (stream *standard-output*))
         (maybe-check-entity entity)
         (let* ((component-storages (storage-component-storages *storage*))
                (soa (svref component-storages ,index-var-name)))
           (declare (ignorable soa))
           (prin1
            (list
             ,(make-keyword name)
             ,@(mapcan #'(lambda (n type)
                           `(,(make-keyword n)
                             (aref
                              (the (simple-array ,type)
                                   (,(symbolicate :% struct-name :- n) soa))
                              entity)))
                slot-names slot-types))
            stream)))
       ,@(remove
          nil
          (mapcar
           #'(lambda (s type i u)
               (%index-accessor-decls
                name index-var-name struct-name s type i u))
           slot-names slot-types slot-index slot-unique))
       ,(%composite-index-accessor-decls composite-index-name name
                                         composite-index-slots
                                         composite-index-unique
                                         slot-names slot-defaults slot-types)
       (defun ,(symbolicate name :-count) ()
         (let* ((component-storages (storage-component-storages *storage*))
                (soa (svref component-storages ,index-var-name)))
           (component-soa-count soa)))
       ',name)))

(defmacro define-component (name &rest slots)
  "An alias for `DEFCOMPONENT`."
  `(defcomponent ,name ,@slots))
